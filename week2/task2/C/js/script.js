let canvas = document.getElementById("pyramidCanvas");
let context = canvas.getContext("2d");

const drawFigure = (x, y) => {
  context.fillStyle = '#ff0000';
  context.fillRect(x, y, 10, 10);
};

const drawPyramid = (n) => {
  context.clearRect(0, 0, canvas.width, canvas.height);

  for (let x = 0; x < n * 10; x += 10) {
    for (let y = x; y < n * 10; y += 10) {
      drawFigure(285 + 25 + x * 1.5, y * 1.5);
      drawFigure(310 - 25 - x * 1.5, y * 1.5);
    }
  }
};

const draw = () => {
  let n = parseFloat(document.getElementById('inputValue').value);
  drawPyramid(n);
};