const sqrt = (x) => {
  const accuracy = 10e-6
  let guess = x / 2;
  while (Math.abs(x - (guess * guess)) > accuracy) {
    guess = (guess + (x / guess)) / 2;
  }
  return guess;
};

const calculateSquareRoot = () => {
  let inputNumber = document.getElementById("inputNumber").value;

  if (inputNumber < 0) {
    document.getElementById("result").textContent = "Введите положительное число";
  } else {
    document.getElementById("result").textContent = "Квадратный корень равен: " + sqrt(inputNumber).toFixed(4);
  }
};