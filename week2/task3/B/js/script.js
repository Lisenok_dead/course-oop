function calculateMedian() {
    var input = document.getElementById("inputArray").value;
    var numbers = input.split(" ").map(function (num) {
        return Number(num.trim());
    });

    numbers.sort(function (a, b) {
        return a - b;
    });

    var median;
    var middleIndex = Math.floor(numbers.length / 2);

    if (numbers.length % 2 === 0) {
        median = (numbers[middleIndex - 1] + numbers[middleIndex]) / 2;
    } else {
        median = numbers[middleIndex];
    }

    document.getElementById('result').innerHTML = "Медиана равна: " + median;
}
