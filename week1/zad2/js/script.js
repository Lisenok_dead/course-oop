//  2
// Типы данных
var number = 2567; // Число
var string = "Hello"; // Строка
var boolean = true; // Логический
var object = {}; // Тип данных "объект"
var array = []; // Тип данных "Массив"

// Вывод типов данных 
console.log(typeof number); // "number"
console.log(typeof string); // "string"
console.log(typeof boolean); // "boolean"
console.log(typeof object); // "object"
console.log(typeof array); // "object"

// Максимальные и минимальные значения числовых типов данных
console.log(Number.MAX_VALUE); // Макс. значение числового типа данных
console.log(Number.MIN_VALUE); // Мин. положительное значение числового типа данных
