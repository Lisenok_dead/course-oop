const isValid = (input: string): boolean => {
  const openingBrackets = new Set(['(', '{', '[']);
  const closingBrackets = new Set([')', '}', ']']);
  const matchingBrackets = new Map([
    ['(', ')'],
    ['{', '}'],
    ['[', ']'],
  ]);

  const queue: string[] = [];

  for (const char of input) {
    if (openingBrackets.has(char)) {
      queue.push(char);
    } else if (closingBrackets.has(char)) {
      const lastChar = queue.pop();
      if (lastChar !== undefined && matchingBrackets.get(lastChar) !== char) {
        return false;
      }
    }
  }

  return queue.length === 0;
};

const getInput = (): string => {
  const bracketInput = document.getElementById('bracketInput') as HTMLInputElement;
  return bracketInput.value;
};

const outputResult = (isValidBracket: boolean): void => {
  const resultDiv = document.getElementById('result') as HTMLDivElement;
  resultDiv.textContent = isValidBracket ? 'Верно' : 'Неверно';
};

const bracketForm = document.getElementById('bracketForm') as HTMLFormElement;
bracketForm.addEventListener('submit', (event) => {
  event.preventDefault();
  const inputString = getInput();
  const isValidBracket = isValid(inputString);
  outputResult(isValidBracket);
});