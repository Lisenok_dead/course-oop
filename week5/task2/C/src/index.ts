const getInputNumber = (): number => {
  const inputElement = document.getElementById('number') as HTMLInputElement;
  return parseInt(inputElement.value);
};

// Функция для вывода результата в поле вывода
const displayResult = (result: number[], method: string) => {
  const outputElement = document.getElementById('output') as HTMLDivElement;
  outputElement.innerHTML = `Последовательность чисел Фибоначчи (${method}): ${result.join(', ')}`;
};

// Функция для рекурсивного вычисления числа Фибоначчи
const fibonacciRecursive = (n: number): number => {
  if (n <= 1) {
    return n;
  }
  return fibonacciRecursive(n - 1) + fibonacciRecursive(n - 2);
};

// Функция для оптимизированного вычисления числа Фибоначчи с кэшированием
const fibonacciOptimized = (n: number, cache: Map<number, number>): number => {
  if (cache.has(n)) {
    return cache.get(n)!;
  }
  if (n <= 1) {
    return n;
  }
  const result = fibonacciOptimized(n - 1, cache) + fibonacciOptimized(n - 2, cache);
  cache.set(n, result);
  return result;
};

// Функция для выполнения расчетов и вывода результатов с помощью рекурсивного метода
const calculateFibonacciRecursive = () => {
  const number = getInputNumber();
  const recursiveResult = Array.from({ length: number }, (_, i) => fibonacciRecursive(i));
  displayResult(recursiveResult, 'рекурсивно');
};

// Функция для выполнения расчетов и вывода результатов с помощью оптимизированного метода
const calculateFibonacciOptimized = () => {
  const number = getInputNumber();
  const cache = new Map<number, number>();
  const optimizedResult = Array.from({ length: number }, (_, i) => fibonacciOptimized(i, cache));
  displayResult(optimizedResult, 'оптимизированно');
};